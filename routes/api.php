<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {

    Route::post('signup', [AuthController::class,'signUp']);
    Route::post('login', [AuthController::class,'login']);
    // Route::post('verificar/{codigo}', 'AuthController@verificarEmail');

    // Route::post('solicitud-reset-password', 'AuthController@solicituResetPassword');
    // Route::get('find-password-reset/{token}', 'AuthController@buscarSolicitudResetPassword');
    // Route::post('reset-password', 'AuthController@resetPassword');




    Route::group([
        'middleware' => ['auth:api']
    ], function () {
        Route::get('user',[AuthController::class,'user']);
        // Route::get('logout',[AuthController::class,'logout']);
    });
});


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
