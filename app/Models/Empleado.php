<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class Empleado extends Authenticatable
{
    use HasApiTokens,HasFactory, Notifiable;

    protected $table = "empleado";
    protected $primaryKey = 'id_empleado';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre_empleado',
        'apellidos_empleado',
        'puesto_empleado',
        'telefono_empleado',
        'contrasenia',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'contrasenia',
    ];


    public function findForPassport($username)
    {
        return $this->where('id_empleado', $username)->first();
    }

    public function validateForPassportPasswordGrant($password)
    {
        return Hash::check($password, $this->contrasenia);
    }

}
