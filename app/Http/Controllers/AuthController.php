<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\Empleado;

class AuthController extends Controller
{

    /**
     * Registro de usuario
     */
    public function signUp(Request $request)
    {

        $rules = [
            'nombre_empleado' => ['required', 'string', 'max:60'],
            'apellidos_empleado' => ['required', 'string', 'max:100'],
            'puesto_empleado' => ['required', 'string', 'max:60'],
            'telefono_empleado' => ['required', 'string', 'min:10'],
            'contrasenia' => ['required', 'string', 'max:50', 'confirmed'],
        ];

        $niceNames =
            [
                'nombre_empleado' => 'nombre del empleado',
                'apellidos_empleado' => 'apellidos del empleado',
                'puesto_empleado' => 'puesto del empleado',
                'telefono_empleado' => 'telefono del empleado',
                'contrasenia' => 'contraseña del empleado'
            ];


        $validator = Validator::make($request->all(), $rules, [], $niceNames);


        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }



        DB::beginTransaction();

        try {


            $usuario = new Empleado();
            $usuario->nombre_empleado       = $request->nombre_empleado;
            $usuario->apellidos_empleado    = $request->apellidos_empleado;
            $usuario->puesto_empleado        = $request->puesto_empleado;
            $usuario->telefono_empleado        = $request->telefono_empleado;
            $usuario->contrasenia       = Hash::make($request->contrasenia);
            $usuario->save();

            DB::commit();
            return Response::json(['mensaje' => 'El empleado ha sido creado!'], 201);
        } catch (\Throwable $th) {
            DB::rollback();

            return Response::json(['mensaje' => 'Ha ocurrido un error!'], 500);
        }
    }

    /**
     * Inicio de sesión y creación de token
     */
    public function login(Request $request)
    {
        $request->validate([
            'id_empleado' => 'required|int',
            'contrasenia' => 'required|string',
        ]);

        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->post(config('services.passport.login_endpoint'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->id_empleado,
                    'password' => $request->contrasenia,
                    'scope' => '*',
                ]
            ]);


            return $response->getBody();
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if ($e->getCode() === 400 || $e->getCode() === 401) {
                return Response::json(['mensaje' => 'Crendenciales incorrectas'], 401);
            }
            return Response::json(['mensaje' => 'Ha ocurrido un error en el servidor.'], 400);
        }
    }

    /**
     * Cierre de sesión (anular el token)
     */
    public function logout(Request $request)
    {

        $accessToken = $request->user()->token();

        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->delete();


        $accessToken->revoke();
        $accessToken->delete();


        // auth()->user()->tokens->each(function ($token, $key) {
        //     $token->delete();
        // });
        return Response::json([
            'mensaje' => 'Has cerrado sesión', 200
        ]);
    }

    /**
     * Obtener el objeto User como json
     */
    public function user(Request $request)
    {
        return Response::json($request->user(), 200);
    }
}
